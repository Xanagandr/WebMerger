#!/usr/bin/env python

from urllib.request import urlopen
import argparse
from bs4 import BeautifulSoup

parser = argparse.ArgumentParser()
parser.add_argument('urls',
                    help='URLs to be merged, separated by a space. Do not forget the protocol!')
parser.add_argument('--strings_only',
                    action="store_true",
                    help="Just store the text for strings in HTML files")
args = parser.parse_args()

urls2merge = args.urls.split(' ')

heads = ''
bodies = ''

for url in urls2merge:
    try:
        web = urlopen(url)
    except ValueError:
        print('%s: not a valid URL (Maybe missing an http/https?)' % url)
        urls2merge.remove(url)

    htmlcode = str(web.read())
    soup = BeautifulSoup(htmlcode, "html5lib")

    if args.strings_only:
        for string in soup.head.stripped_strings:
            heads += repr(string)

        for string in soup.body.stripped_strings:
            bodies += repr(string)

    else:
        for child in soup.head.descendants:
            if type(child) != 'script':
                heads += str(child)

        for child in soup.body.descendants:
            if type(child) != 'script':
                bodies += str(child)

newsoup = BeautifulSoup('<html>' + heads + bodies + '</html>', "html5lib")

file = open("mergedwebpages.html", "w")
file.write(newsoup.prettify())
file.close()